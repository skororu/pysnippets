# Scrape

These scripts examine the frequency of words in the titles of a random sample (with replacement) of [XKCD](https://xkcd.com) cartoons.

There are three versions of the same basic task. The first is single threaded and generator based, which is effective but relatively slow. The second uses a thread pool (quick but a little resource heavy), and the third uses asyncio (also quick, but should use fewer resources than the thread pool).

##### How to get them working

These scripts are intended for Python 3.6 as they use [f-strings](https://www.python.org/dev/peps/pep-0498/).

Make sure you've installed `lxml` for Beautiful Soup to use. Run using `single.py`, `threads.py` or `aio.py`, and a summary will be echoed to the terminal:

```
$ ./aio.py 
The titles from 80 randomly selected XKCD cartoons contained 147 unique words.
8 words were used more than once:
a
crawl
election
new
opening
so
the
up
```
