#!/usr/bin/env python3
"""
A simple web scraping test using threads:
Examine the word frequency of the titles from a random sample of XKCD cartoons
"""

import collections as col                           # Counter
from multiprocessing.pool import ThreadPool as Pool # imap_unordered
import random                                       # randint

import bs4                                          # BeautifulSoup
import requests                                     # codes.ok, get

def generate_urls(base, limit, quantity):
    """
    generate a series of URLs, each of which represents a randomly selected,
    valid XKCD cartoon webpage
    """
    for url_num in range(quantity):
        yield f'{base}{random.randint(1, limit)}/'

def get_title(url):
    """
    get the title string of a supplied URL
    """
    req = requests.get(url)
    page = bs4.BeautifulSoup(req.text, 'lxml')
    return page.find(id='ctitle').text

def get_words(urls, quantity):
    """
    count the unique words from the title strings of all the URLs given
    and provide some basic summary statistics
    """
    word_freq = col.Counter()

    # asynchronously fetch webpage titles and fragment them into words
    pool = Pool(20)
    for title in pool.imap_unordered(get_title, urls):
        word_freq.update(col.Counter(title.lower().split(' ')))

    # display overview
    multiple = len([x for x in word_freq.values() if x > 1])
    print(f'The titles from {quantity} randomly selected XKCD cartoons contained '
          f'{len(word_freq)} unique words.\n'
          f'{multiple} words were used more than once:')

    # display frequent words
    frequent_words = sorted(k for k, v in word_freq.items() if v > 1)
    for word in frequent_words:
        print(word)

def main():
    """simple web scraping test"""
    quantity = 80
    base_url = 'http://xkcd.com/'

    # obtain integer value of most recent cartoon
    # so our random selection has an upper bound
    with requests.get(base_url) as req:
        page = bs4.BeautifulSoup(req.text, 'lxml')
        num_previous = page.find('a', rel='prev')['href']
        upper_limit = int(num_previous[1:-1]) + 1

    # asynchronously handle IO when fetching multiple URLs
    get_words(generate_urls(base_url, upper_limit, quantity), quantity)


##############################################################################
if __name__ == '__main__':
    main()
