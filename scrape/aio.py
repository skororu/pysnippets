#!/usr/bin/env python3
"""
A simple web scraping test using asyncio:
Examine the word frequency of the titles from a random sample of XKCD cartoons
"""

import asyncio            # gather, get_event_loop
import collections as col # Counter
import functools          # partial
import random             # randint

import aiohttp            # ClientSession
import async_timeout      # timeout
import bs4                # BeautifulSoup
import requests           # get

def generate_urls(base, limit, quantity):
    """
    generate a series of URLs, each of which represents a randomly selected,
    valid XKCD cartoon webpage
    """
    for url_num in range(quantity):
        yield f'{base}{random.randint(1, limit)}/'

async def get_title(session, url):
    """
    get the title string of a supplied URL
    """
    with async_timeout.timeout(10):
        async with session.get(url) as response:
            req = await response.text()
            return bs4.BeautifulSoup(req, 'lxml').find(id='ctitle').text

async def _get_words(urls, quantity, session):
    """
    count the unique words from the title strings of all the URLs given
    and provide some basic summary statistics
    """
    word_freq = col.Counter()

    # asynchronously fetch title words and note their frequency
    tasks = (get_title(session, url) for url in urls)
    for title in await asyncio.gather(*tasks):
        word_freq.update(col.Counter(title.lower().split(' ')))

    # display overview
    multiple = len([x for x in word_freq.values() if x > 1])
    print(f'The titles from {quantity} randomly selected XKCD cartoons contained '
          f'{len(word_freq)} unique words.\n'
          f'{multiple} words were used more than once:')

    # display frequent words
    frequent_words = sorted(k for k, v in word_freq.items() if v > 1)
    for word in frequent_words:
        print(word)

def main():
    """simple web scraping test"""
    quantity = 80
    base_url = 'http://xkcd.com/'

    # obtain integer value of most recent cartoon
    # so our random selection has an upper bound
    with requests.get(base_url) as req:
        page = bs4.BeautifulSoup(req.text, 'lxml')
        num_previous = page.find('a', rel='prev')['href']
        upper_limit = int(num_previous[1:-1]) + 1

    # asynchronously handle IO when fetching multiple URLs
    loop = asyncio.get_event_loop()
    with aiohttp.ClientSession(loop=loop) as session:
        get_words = functools.partial(_get_words, quantity=quantity, session=session)
        loop.run_until_complete(get_words(generate_urls(base_url, upper_limit, quantity)))


##############################################################################
if __name__ == '__main__':
    main()
