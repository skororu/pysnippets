# ZeroMQ

This Python snippet was the prototype for allowing the user to interact with the [Distributed single frame render for Blender Cycles](https://gitlab.com/skororu/dtr) while long-running render processes were running. Specifically, the intention was to allow the user to temporarily suspend CPU-intensive 3D rendering on a given remote node - if another user needed to use the node for other tasks - and then allow the node to be made active again afterwards.

`zmq_client.py` provides a command line interface (CLI) to communicate with `zmq_server.py`. The CLI retains a record of previously entered commands (accessible with up and down arrow keys) and will tab complete commands it recognises.

##### How to get it working

Run each script in the foreground in a separate terminal window on the same machine. Then on the client, use the command `nodes` first - this queries the server for the IP addresses it is managing, and enters those into the client's tab completion vocabulary.  You can then `pause <ip address>` and `resume <ip address>` to simulate pausing and restarting the render nodes.

Use `q`, `quit` or `quit()` to exit the client.  Use a single press of `ctrl-c` to exit the server.  In both cases, exceptions are caught to allow the script to tidy up the ZeroMQ connections on exit.
