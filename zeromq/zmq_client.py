#!/usr/bin/env python3
"""ZeroMQ test client with tab completion"""

import readline              # parse_and_bind, set_completer
import zmq                   # Context, REQ

import zmq_data_struct as zs # RenderNode, UserExit


##############################################################################
def main():
    """ZeroMQ test client"""

    # initialise tab completion
    vocab_ref = ['pause', 'resume', 'nodes', 'quit']
    vocab = list(vocab_ref)

    def complete(text, state):
        """tab completion"""
        results = [x for x in vocab if x.startswith(text)] + [None]
        return results[state]

    readline.parse_and_bind("tab: complete")
    readline.set_completer(complete)

    # initialise ZeroMQ
    context = zmq.Context()
    socket = context.socket(zmq.REQ)
    socket.connect("tcp://localhost:5555")

    # user help
    print('** ZeroMQ test client with command autocomplete **')
    print()
    print('Run zmq_server.py and zmq_client.py in different terminal windows on the same machine')
    print()
    print('Commands available: pause <ip address>')
    print('                    resume <ip address>')
    print('                    nodes')
    print('                    quit')
    print()
    print('You should run the nodes command first, this will fetch a list of nodes')
    print('from the server and then this client will be able to autocomplete the')
    print('ip addresses as well as the commands.')
    print()

    # (1) handle input of user commands with tab completion
    # (2) send them to the server (server.py) and
    # (3) display returned replies
    finished = False
    user_action = zs.UserExit()
    while not user_action.interrupt and not finished:
        line = input('command: ')

        str_tidy = [x.strip() for x in line.partition(' ')]
        command, _, argument = str_tidy

        if command in ['q', 'quit', 'quit()']:
            finished = True
            continue

        command_malformed = str_tidy[0] == '' or str_tidy[-1] == ''
        if command_malformed and (command == 'pause' or command == 'resume'):
            print('use one of the IP addresses provided by the nodes command')
            print('syntax:', command, ' <ip address>')
            continue

        if command in ['pause', 'resume', 'nodes']:
            # send command
            socket.send_string(command + '=' + argument, encoding='UTf-8')

            # receive response (this should block)
            message = socket.recv().decode('UTF-8')

            if command == 'nodes':
                mess = message.split(',')
                vocab = vocab_ref + mess
                print('tab completion recognises:', ', '.join(sorted(x for x in vocab)))
            else:
                print('server says:', message)


##############################################################################
if __name__ == '__main__':
    main()
