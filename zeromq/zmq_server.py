#!/usr/bin/env python3
"""ZeroMQ test server"""

import time                  # sleep
import zmq                   # Context

import zmq_data_struct as zs # RenderNode, UserExit


##############################################################################
# interaction with remote command script
##############################################################################

def chk_command_pause(node):
    """
    process user request to pause node, provides a service to check_command()

    --------------------------------------------------------------------------
    args
        node : instance of RenderNode
    --------------------------------------------------------------------------
    returns
        resp : string
    --------------------------------------------------------------------------
    """
    if node.active:
        resp = node.ip_address + ' will be paused'
        node.active = False
    else:
        resp = node.ip_address + ' is already paused'

    return resp

def chk_command_resume(node):
    """
    process user request to resume node, provides a service to check_command()

    --------------------------------------------------------------------------
    args
        node : instance of RenderNode
    --------------------------------------------------------------------------
    returns
        resp : string
    --------------------------------------------------------------------------
    """
    if not node.active:
        resp = node.ip_address + ' will be reactivated'
        node.active = True
    else:
        resp = node.ip_address + ' is already active'

    return resp

def check_command(render_nodes, socket):
    """
    need to be careful with decoding the byte sequences.  as we can't be sure
    the script issuing commands will be on the same system as this script, we
    should avoid using sys.stdout.encoding, and manually assert 'UTF-8' both
    here and in the command issuer

    --------------------------------------------------------------------------
    args
        render_nodes : list of RenderNode instances
            contains information about all the nodes in the cluster
        socket : zmq.sugar.socket.Socket
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    try:
        message = socket.recv(flags=zmq.NOBLOCK)
    except zmq.error.Again:
        pass
    else:
        # a command has been received from external script
        chk_command = {
            'pause': chk_command_pause,
            'resume': chk_command_resume
        }
        reply_text = ''
        var_name, _, value = message.decode('utf-8', 'ignore').partition('=')
        node = next((x for x in render_nodes if x.ip_address == value), None)

        # take action appropriate to the received command,
        # where that command refers to a node
        if node:
            if chk_command.get(var_name):
                reply_text = chk_command[var_name](node)
                print(reply_text)

        elif var_name in ['pause', 'resume']:
            reply_text = 'unrecognised ip address'
            print(reply_text)

        # reply with list of nodes, used to help with tab completion
        elif var_name == 'nodes':
            reply_text = ','.join(x.ip_address for x in render_nodes)
            print('request for node list received')


        socket.send_string(reply_text)

def init_zmq():
    """
    initialise communication with external Python scripts using zmq

    --------------------------------------------------------------------------
    args : none
    --------------------------------------------------------------------------
    returns
        socket : zmq.sugar.socket.Socket
    --------------------------------------------------------------------------
    """
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    socket.bind("tcp://*:5555")

    return socket


##############################################################################
def main():
    """ZeroMQ test server"""

    # set up some fake node IP addresses for reference
    test_nodes = []
    test_nodes.append(zs.RenderNode('192.168.0.14'))
    test_nodes.append(zs.RenderNode('192.168.0.19'))
    test_nodes.append(zs.RenderNode('192.168.0.10'))

    # user help
    print('** ZeroMQ test server **')
    print()
    print('Run zmq_server.py and zmq_client.py in different terminal windows on the same machine')
    print()
    print('Test node IP addresses are:')
    print(', '.join(sorted(x.ip_address for x in test_nodes)))
    print()
    print('Press ctrl-c once to exit')
    print()

    # allow this script to receive instructions from an external script
    socket = init_zmq()

    # main loop - need to handle user interrupts gracefully
    # so the script can tidy up the open ZeroMQ connection on exit
    user_action = zs.UserExit()
    while not user_action.interrupt:
        # check if we have received a command from an external script
        check_command(test_nodes, socket)

        # sleep for a short while before polling again
        time.sleep(0.5)


##############################################################################
if __name__ == '__main__':
    main()
