"""
contains data structures used by the project
"""

import signal                # signal

class RenderNode:
    """define nodes available to render tiles"""
    def __init__(self, ip_address, active=True):
        self.ip_address = ip_address            # ip address of node
        self.active = active                    # the state of this render node: active or paused


class UserExit:
    """handle user interrupts gracefully"""
    interrupt = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        """set flag indicating user interrupt"""
        self.interrupt = True
