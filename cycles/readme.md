# cycles

When performing a distributed image render with Blender, as an alternative to splitting the image into blocks, it is possible to distribute ranges of sample values instead.  Here is a simple (non-distributed) example of rendering multiple image chunks and then combining them to form a final composite image.

##### How to get it working

Check that the variable `binloc` in `cycles.py` is set correctly for your system/OS, then run using `./cycles.py`.

The chunks will be saved to the [chunks](https://gitlab.com/skororu/pysnippets/tree/master/cycles/chunks) directory as they are rendered. These are then combined into the final file `composite.png`.

##### General command line solution

The above approach is specific to merging chunks formed from 8-bit PNG files. For a more general approach using [GNU Parallel](https://www.gnu.org/software/parallel/) and [ImageMagick](https://imagemagick.org/script/convert.php), see [here](https://gitlab.com/skororu/scripts#render).
