#!/usr/bin/env python3
"""
Example image render using chunks; requires Blender 2.78 or above.

Tested on macOS Sierra, check the value of binloc is appropriate for your
system/OS.

See: Resumable render implementation for Cycles
https://developer.blender.org/rBf8b9f4e
"""

import subprocess               # call

import numpy                    # asarray
import PIL.Image                # open


def render_chunk(chunks, chunk):
    """
    render chunk on local machine, where a chunk represents a range of samples
    for the whole image

    --------------------------------------------------------------------------
    args
        chunks : int
            how many chunks in total will be rendered
        chunk : int
            the chunk to render this time
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    print('rendering chunk', chunk, 'of', chunks)

    # check this for your system/OS
    binloc = '/Applications/Blender/blender.app/Contents/MacOS/blender'

    filename_render = 'render.blend'
    filename_python = 'config.py'
    filename_out = '//chunks/chunk_' + str(chunk).zfill(len(str(chunks))) + '.png'
    command = [binloc, \
        '-b', filename_render, \
        '-o', filename_out, \
        '--python', filename_python, \
        '--', \
        '--cycles-resumable-num-chunks', str(chunks), \
        '--cycles-resumable-current-chunk', str(chunk)]
    render_the_chunk = ' '.join(command)

    subprocess.call(render_the_chunk, \
        shell=True, \
        stdout=subprocess.DEVNULL, \
        stderr=subprocess.DEVNULL)

def create_final_image_from_chunks(chunks):
    """
    Sum the rendered chunks together to form the final image

    The accumulator uses uint32 arrays. Given chunks rendered as
    16bpp PNG images, this would allow the summing of over
    a quarter of a million chunks before an overflow occured.
    It is unlikely that more than a few hundred will ever be used.

    --------------------------------------------------------------------------
    args
        chunks : int
            how many chunks in total will be rendered
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    padding = len(str(chunks))

    # set up accumulator
    tmp = PIL.Image.open('chunks/chunk_' + str(1).zfill(padding) + '.png')
    accumulator = numpy.asarray(tmp).astype('uint32')

    # combine chunks
    for chunk in range(2, chunks + 1):
        with PIL.Image.open('chunks/chunk_' + str(chunk).zfill(padding) + '.png') as img:
            accumulator += numpy.asarray(img).astype('uint8')

    # save final image
    final = PIL.Image.fromarray((accumulator // chunks).astype('uint8'))
    final.save('composite.png')


##############################################################################
def main():
    """render image"""
    chunks = 16

    # render all chunks
    for chunk in range(1, chunks + 1):
        render_chunk(chunks, chunk)

    # sum the images together
    create_final_image_from_chunks(chunks)


##############################################################################
if __name__ == '__main__':
    main()
