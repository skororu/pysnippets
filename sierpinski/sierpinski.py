#!/usr/bin/env python3
"""
Render Sierpinski triangle pattern to a PNG file
"""

import numpy                 # fromfunction, where
import PIL.Image             # fromarray

def sierpinski(xpc, ypc):
    """
    generate Sierpinski triangle values

    --------------------------------------------------------------------------
    args
        xpc : int
            (x) (p)ixel (c)oordinate
        ypc : int
            (y) (p)ixel (c)oordinate
    --------------------------------------------------------------------------
    returns
        uint8
            either 0 or 255, representing a black or white pixel respectively
    --------------------------------------------------------------------------
    """
    return numpy.where(xpc & ypc, 255, 0).astype('uint8')

def main():
    """create numpy array with greyscale values, write to monochrome PNG"""

    # create 256 x 256 numpy uint8 array and fill with Sierpinski pattern
    buffer = numpy.fromfunction(sierpinski, (256, 256), dtype='uint8')

    # convert from greyscale (8bpp) to monochrome (1bpp), and save
    png = PIL.Image.fromarray(buffer, 'L').convert('1')
    png.save('sierpinski.png')


##############################################################################
if __name__ == '__main__':
    main()
