# Sierpinski

This uses a shortcut to draw the Sierpinski triangle, performing a bitwise `and` on all the x and y coordinate pairs. When the result is zero a black pixel is set, any other value produces a white pixel.

##### How to get it working

Run using `./sierpinski.py && open sierpinski.png`.
