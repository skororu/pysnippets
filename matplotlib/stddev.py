#!/usr/bin/env python3
"""
Based on a notional cluster with 4 nodes, where in each of 1000 runs
each node was allocated a random benchmark result (within reasonable bounds)

Given this data, this graph illustrates the relationship between:

* normalised overall render time of the whole cluster (lower figures are better)
* the standard deviation of the benchmark performance of all nodes in the cluster
* the frequency a performance

In essence, when a cluster is comprised of nodes that have broadly similar
performance, the result is short render times (idle time at the end of the
render is kept to a minumum).

When a cluster is comprised of nodes with widely differing levels of
performance, the result is long render times (there is substantial idle time
at the end of the render, where most nodes have completed their allocated work
and are idle while one or two remain busy).

An x-axis figure of 1.0 would indicate the time taken to render the image on
the single fastest node alone. A figure of 0.25 would indicate 4 equally fast
nodes that complete the work in a quarter of the time it would take the
fastest node to render the image on its own.
"""

import pickle                    # dump, load
import statistics                # mean
import matplotlib.pyplot as plt
import numpy as np               # arange, array, poly1d, polyfit


def main():
    """dual axis graph"""

    # load results from 1000 data points
    #
    # cluster_rrt is a list of relative render times
    # cluster_sd is a list containing the standard deviation of node benchmarks
    infile = open('stddev.p', 'rb')
    cluster_rrt, cluster_sd = pickle.load(infile)
    infile.close()

    step = 0.05
    start = 0.25
    end = 0.85

    # define bin start and end points
    bs_st = np.arange(start, end-step, step)
    bs_en = np.arange(start+step, end, step)

    # build the core data structure with the the bin bounds, number of values,
    # list of standard deviations
    br = []
    for bin_lower, bin_upper in zip(bs_st, bs_en):
        br.append([bin_lower, bin_upper, 0, []])

    # collate the number of values that are within bins (frequency),
    # and the std deviations that apply to them
    for i, rrt in enumerate(cluster_rrt):
        for h in br:
            if rrt >= h[0] and rrt < h[1]:
                h[2] += 1
                h[3].append(cluster_sd[i])

    # extract the frequency and standard deviation data for plotting
    s_binned = [0.0 if x[2] == 0 else statistics.mean(x[3]) for x in br]
    f_binned = [x[2] for x in br]


    ################################################################################
    # plot
    ################################################################################

    width = 0.25
    x_offset = 0.6
    ind = list(range(len(f_binned)))
    indw = [x + x_offset + width for x in ind]
    indo = [x + x_offset for x in ind]
    x_labels = [str(x) + ' - ' + str(y) for x, y in zip(bs_st, bs_en)]

    frq_col = (128/255, 128/255, 128/255)
    std_col = (61/255, 17/255, 123/255)

    ################################################################################
    # frequency
    ################################################################################

    fig1, ax1 = plt.subplots(figsize=(10, 4))
    ax1.bar(indo, f_binned, width, color=frq_col, linewidth=0, label='frequency')
    ax1.set_xlabel('relative render time (lower values are better, 0.25 is optimal)', labelpad=12)
    ax1.set_ylabel('frequency', color=frq_col)
    for tl in ax1.get_yticklabels():
        tl.set_color(frq_col)

    plt.ylim(0, 200)
    yt = list(range(0, 200 + 1, 100))
    yts = [str(x) for x in yt]
    plt.yticks(yt, yts)

    ################################################################################
    # standard deviation
    ################################################################################

    x = np.array(ind)
    y = np.array(s_binned)
    z = np.polyfit(x, y, 3)
    p = np.poly1d(z)

    ax2 = ax1.twinx()
    ax2.bar(indw, p(x), width, color=std_col, linewidth=0, label='standard deviation')

    ax2.set_ylabel('standard deviation of\ncluster benchmarks', color=std_col)
    for tl in ax2.get_yticklabels():
        tl.set_color(std_col)

    plt.ylim(0, 40)
    yt = list(range(0, 40 + 1, 20))
    yts = [str(x) for x in yt]
    plt.yticks(yt, yts)

    # x axis labels - bands
    ax1.set_xticks(indw)
    ax1.set_xticklabels(x_labels, rotation=45)

    ################################################################################
    # overall title
    ################################################################################
    fig1.suptitle('the effect of performance differences within a 4 node cluster\n \
        on relative render time (1000 data points)', y=0.97, fontsize=14)
    plt.subplots_adjust(top=0.85, bottom=0.3)

    ################################################################################
    # get rid of chartjunk
    ################################################################################

    ax1.spines['top'].set_visible(False)
    ax1.xaxis.set_ticks_position('bottom')

    ax2.spines['top'].set_visible(False)
    ax2.xaxis.set_ticks_position('bottom')

    ################################################################################
    # plot and save
    ################################################################################
    plt.savefig('stddev.png', dpi=200)


if __name__ == '__main__':
    main()
