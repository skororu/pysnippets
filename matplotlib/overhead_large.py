#!/usr/bin/env python3
"""
illustrate the overheads incurred by the script when rendering a small image

plot a mean overhead figure with 2 subplots (time and percentage)
for a 500x200 image, when using 1 to 8 tiles
"""

import statistics as stat
import matplotlib.pyplot as plt

# times from command line render, image resolution 500x200
# using: ./blender -b render.blend -f 1
#
cl_rend_times = [3721, 3732, 3717, 3719, 3704, 3711, 3703, 3713, 3709, 3716]
cl_render_time = round(stat.mean(cl_rend_times))

# times from using the script, image resolution 500x200
# test results for 1 to 8 tiles, with each iterated 10 times
#
t_data = [
    [3713, 3724, 3768, 3744, 3728, 3722, 3724, 3730, 3732, 3732], # times for 1 tile
    [3738, 3720, 3711, 3727, 3713, 3713, 3708, 3710, 3713, 3715], # times for 2 tiles
    [3771, 3750, 3756, 3762, 3762, 3758, 3767, 3748, 3787, 3771], # times for 3 tiles
    [3749, 3758, 3727, 3729, 3729, 3727, 3725, 3731, 3735, 3725],
    [3829, 3741, 3815, 3791, 3835, 3813, 3753, 3767, 3754, 3767],
    [3823, 3822, 3787, 3753, 3757, 3759, 3756, 3752, 3760, 3793],
    [3786, 3758, 3791, 3759, 3755, 3795, 3759, 3795, 3793, 3823],
    [3816, 3792, 3848, 3855, 3853, 3834, 3826, 3804, 3776, 3775]  # times for 8 tiles
]


if __name__ == '__main__':
    t_overhead = []
    pc_overhead = []

    num_tiles = len(t_data)

    for n_tiles in t_data:
        overhead = stat.mean(n_tiles) - cl_render_time
        oh_pc = (overhead / cl_render_time) * 100
        pc_overhead.append(oh_pc)
        t_overhead.append(overhead)

    x_offset = 0.25
    width = 0.5
    t_normal = [cl_render_time] * num_tiles
    ind = [x_offset + x for x in range(num_tiles)]
    ind_position = [x + width/2.0 for x in ind]
    labels = [str(x) for x in range(1, num_tiles + 1)]
    mean_max = max([stat.mean(x) for x in t_data])

    fig = plt.figure(figsize=(12, 4))

    ################################################################################
    # plot 1 left
    ################################################################################

    plt.subplot(1, 2, 1)

    # y values for stacking are in t_normal and t_overhead
    p1 = plt.bar(ind, t_normal, width, color='#223d4a', linewidth=0)
    p2 = plt.bar(ind, t_overhead, width, color='#45b89d', bottom=t_normal, linewidth=0)

    plt.ylim(3698, 3840)
    plt.xticks(ind_position, labels)
    plt.yticks([cl_render_time, int(mean_max)], [str(cl_render_time), str(int(mean_max))])
    plt.ylabel('time (s)')
    plt.xlabel('blocks')
    plt.title('mean overhead (s)')

    # mean command line render time
    plt.axhline(cl_render_time, color='#aaaaaa', zorder=5)

    # add labels above the bars: mean overhead time in seconds
    ax = plt.gca()
    rects = ax.patches
    for rect, label in zip(rects, t_overhead):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2, height + label + 3, "+{:.1f}s".format(label), \
            ha='center', va='bottom', fontsize=10)

    # get rid of chartjunk
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    plt.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='on')
    plt.tick_params(axis='y', which='both', left='on', right='off', labelleft='on')


    ################################################################################
    # spacing
    ################################################################################
    plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=20)


    ################################################################################
    # plot 2 right
    ################################################################################
    plt.subplot(1, 2, 2)

    p1 = plt.bar(ind, pc_overhead, width, color='#45b89d', linewidth=0)

    plt.xticks(ind_position, labels)
    plt.xlabel('blocks')
    plt.title('mean overhead (%)')

    # add labels above the bars: mean overhead percentage
    ax = plt.gca()
    rects = ax.patches
    for rect, label in zip(rects, pc_overhead):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2, height + 0.05, "{:.1f}%".format(label), \
            ha='center', va='bottom', fontsize=10)

    # get rid of chartjunk
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    ax.spines['left'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    plt.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='on')
    plt.tick_params(axis='y', which='both', left='off', right='off', labelleft='off')

    ################################################################################
    # overall title
    ################################################################################
    fig.suptitle('script overhead (100000 pixel image, single render node)', y=0.97, fontsize=16)
    plt.subplots_adjust(top=0.80, bottom=0.125)

    ################################################################################
    # plot and save
    ################################################################################
    fig.savefig('overhead_large.png', dpi=200)
