#!/usr/bin/env python3
"""
Illustrate the relationship between execution time and the number of
nodes and tiles, for both original and new solving methods when choosing
whether to issue a tile to a newly idle node
"""
import matplotlib.pyplot as plt

# number of nodes, [tiles], [execution times old], [execution times new]]
#
# results[0][2] is a list of execution times for 9, 10, 11, 12 and 13 tiles,
# where nodes=4, using the old method
#
# results[2][3] is a list of execution times for 9, 10, 11, 12 and 13 tiles,
# where nodes=6, using the new method
results = [
    [4, \
        [9, 10, 11, 12, 13], \
        [0.003504, 0.004960, 0.007102, 0.009783, 0.013533], \
        [0.001609, 0.002214, 0.003043, 0.003381, 0.004853]], \
    [5, \
        [9, 10, 11, 12, 13], \
        [0.024759, 0.045443, 0.065444, 0.101739, 0.144700], \
        [0.006531, 0.013709, 0.017701, 0.026912, 0.028325]], \
    [6, \
        [9, 10, 11, 12, 13], \
        [0.203209, 0.397099, 0.700139, 1.171231, 1.883710], \
        [0.019783, 0.033379, 0.045567, 0.071788, 0.104355]]
]

if __name__ == '__main__':

    tile_min = min(results[0][1])
    tile_max = max(results[0][1])

    ################################################################################
    # plot with 3 subplots
    ################################################################################

    width = 0.4
    ind = list(range(tile_min, tile_max + 1))
    indw = [x + width for x in ind]
    labels = [str(x) for x in results[0][1]]

    f, axarr = plt.subplots(1, len(results), sharey=True, figsize=(8, 3.2))
    plt.yticks(range(0, 3), [0, 1, 2])
    plt.ylim(0, 2)

    for ai, i in enumerate(results):

        new = axarr[ai].bar(ind, i[3], width, color=(0.267, 0.220, 0.243), linewidth=0, \
            label='new method')
        old = axarr[ai].bar(indw, i[2], width, color=(0.384, 0.549, 0.522), linewidth=0, \
            label='original')

        axarr[ai].set_xticklabels(labels)
        axarr[ai].set_xticks([x + 0.425 for x in i[1]])
        axarr[ai].set_title(str(i[0]) + ' nodes')
        axarr[ai].set_xlabel('blocks')

        # to the leftmost subplot, include details common to all three subplots
        if ai == 0:
            axarr[ai].set_ylabel('response time (s)')
            legend = axarr[ai].legend(loc='upper center', prop={'size':8})

        # get rid of chartjunk
        axarr[ai].spines["top"].set_visible(False)
        axarr[ai].spines["right"].set_visible(False)

        for t in axarr[ai].yaxis.get_ticklines():
            t.set_visible(False)

        for t in axarr[ai].xaxis.get_ticklines():
            t.set_visible(False)

        if ai > 0:
            axarr[ai].spines["left"].set_visible(False)

    ################################################################################
    # overall title
    ################################################################################
    f.suptitle('execution time from given numbers of nodes and tiles', y=0.97, fontsize=14)
    plt.subplots_adjust(top=0.75, bottom=0.225)

    ################################################################################
    # plot and save
    ################################################################################
    plt.savefig('barplots.png', dpi=200)
