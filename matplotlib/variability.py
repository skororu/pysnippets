#!/usr/bin/env python3
"""
illustrate the variability in total render time for the 500x200 image
to demonstrate that the variability and the overhead
are of similar order
"""

import statistics as stat
import matplotlib.pyplot as plt

# times from command line render,  image resolution 500x200
# using: ./blender -b render.blend -f 1
#
cl_rend_times = [3721, 3732, 3717, 3719, 3704, 3711, 3703, 3713, 3709, 3716]
cl_render_time = round(stat.mean(cl_rend_times))

# times from using the script, image resolution 500x200
# test results for 1 to 8 tiles, with each iterated 10 times
#
timing_data = [
    [3713, 3724, 3768, 3744, 3728, 3722, 3724, 3730, 3732, 3732], # times for 1 tile
    [3738, 3720, 3711, 3727, 3713, 3713, 3708, 3710, 3713, 3715], # times for 2 tiles
    [3771, 3750, 3756, 3762, 3762, 3758, 3767, 3748, 3787, 3771], # times for 3 tiles
    [3749, 3758, 3727, 3729, 3729, 3727, 3725, 3731, 3735, 3725],
    [3829, 3741, 3815, 3791, 3835, 3813, 3753, 3767, 3754, 3767],
    [3823, 3822, 3787, 3753, 3757, 3759, 3756, 3752, 3760, 3793],
    [3786, 3758, 3791, 3759, 3755, 3795, 3759, 3795, 3793, 3823],
    [3816, 3792, 3848, 3855, 3853, 3834, 3826, 3804, 3776, 3775]  # times for 8 tiles
]

if __name__ == '__main__':
    t_overhead = []
    t_mean = []
    error = [[], []]

    num_tiles = len(timing_data)

    for n_tiles in timing_data:
        mean_time_all = stat.mean(n_tiles)
        overhead = mean_time_all - cl_render_time
        t_overhead.append(overhead)

        lower_bound = mean_time_all - min(n_tiles)
        upper_bound = max(n_tiles) - mean_time_all

        t_mean.append(mean_time_all)
        error[0].append(lower_bound)
        error[1].append(upper_bound)

    mean_max = max(t_mean)

    # general plot options
    x_offset = 0.125
    width = 0.25
    label_offset_h = 0.4
    label_offset_v = 5

    # prepare data for plot 1 (left)
    ind = list(range(1, num_tiles + 1))
    ind_position = [x + width/2.0 for x in ind]
    ind_tick = [x + x_offset for x in ind_position]
    labels = [str(x) for x in ind]
    t_normal = [cl_render_time for x in ind]

    fig = plt.figure(figsize=(12, 4))

    ################################################################################
    # plot 1 left
    ################################################################################

    p1 = plt.bar(ind_position, t_mean, width, color='#223d4a', linewidth=0, yerr=error, \
        ecolor='#25987d', capsize=5)

    plt.ylim(3700, 3900)
    plt.yticks([3700, 3800, 3900], ['3700', '3800', '3900'])
    plt.xticks(ind_tick, labels)

    plt.ylabel('time (s)')
    plt.xlabel('blocks')
    plt.title('variability in total render time (100000 pixel image, single render node)')

    ax = plt.gca()

    # label error bars
    v = 0
    rects = ax.patches
    for rect, label in zip(rects, t_overhead):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2 + label_offset_h, \
            height - label_offset_v - error[0][v], \
            '− {:.1f}s'.format(error[0][v]), ha='center', va='bottom')
        ax.text(rect.get_x() + rect.get_width()/2 + label_offset_h, \
            height - label_offset_v + error[1][v], \
            '+ {:.1f}s'.format(error[1][v]), ha='center', va='bottom')
        v += 1

    # get rid of chartjunk
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    plt.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='on')
    plt.tick_params(axis='y', which='both', left='on', right='off', labelleft='on')

    # avoid cropping titles and labels
    plt.tight_layout()

    ################################################################################
    # plot and save
    ################################################################################
    fig.savefig('variability.png', dpi=200)
