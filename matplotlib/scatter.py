#!/usr/bin/env python3
"""
demonstrate that a small change in the number of nodes has a
much greater effect on the response time than a similar change in the
number of blocks
"""
import matplotlib.pyplot as plt

# numbers of tiles, x axis for plot
xc = [8, 9, 10, 11, 12, 8, 9, 10, 11, 12, 8, 9, 10, 11, 12]

# numbers of nodes, y axis for plot
yc = [4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6]

# response times from new method
tc_new_method = [0.001609, 0.002214, 0.003043, 0.003381, 0.004853, # times for 4 nodes
                 0.006531, 0.013709, 0.017701, 0.026912, 0.028325, # times for 5 nodes
                 0.019783, 0.033379, 0.045567, 0.071788, 0.104355] # times for 6 nodes


if __name__ == '__main__':

    node_min = min(yc)
    node_max = max(yc)
    tile_min = min(xc)
    tile_max = max(xc)

    # scale tc_new_method to give us visable and useful circle areas for the chart
    tc = [x * 6000 for x in tc_new_method]

    # adjust colours as well as circle areas
    colours = [(0.384*(x/max(tc)), 0.549*(x/max(tc)), 0.522*(x/max(tc))) for x in tc]

    ################################################################################
    # scatter plot
    ################################################################################

    fig = plt.figure(figsize=(6, 2.5))

    ax = plt.gca()
    ax.set_xticks(range(tile_min, tile_max + 1))
    ax.set_yticks(range(node_min, node_max + 1))
    ax.set_title('relative response time')
    ax.set_ylabel('nodes')
    ax.set_xlabel('blocks')

    plt.scatter(xc, yc, s=tc, c=colours, linewidth=0)

    # get rid of chartjunk
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)

    for t in ax.yaxis.get_ticklines():
        t.set_visible(False)

    for t in ax.xaxis.get_ticklines():
        t.set_visible(False)

    plt.tight_layout()


    ################################################################################
    # plot and save
    ################################################################################
    plt.savefig('scatter.png', dpi=200)
