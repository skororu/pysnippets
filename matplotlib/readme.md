# matplotlib

A few matplotlib examples that are free of clutter and easy to read, using features including:

* multiple subplots
* error bars with offset labels
* dual y-axes
* binning
* curve fitting

##### How to get them working

Run each example with a command like `./barplots.py && open barplots.png`.
