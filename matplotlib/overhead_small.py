#!/usr/bin/env python3
"""
illustrate the overheads incurred by the script when rendering a very small image

plot a mean overhead figure with 2 subplots (time and percentage)
for a 100x100 image, when using 1 to 8 tiles
"""

import statistics as stat
import matplotlib.pyplot as plt

# times from command line render,  image resolution 100x100
# using: ./blender -b render.blend -f 1
#
cl_rend_times = [376, 372, 378, 378, 371, 370, 371, 370, 371, 372]
cl_render_time = round(stat.mean(cl_rend_times))

# times from using the script, image resolution 100x100
# test results for 1 to 8 tiles, with each iterated 9 times
#
timimg_data = [
    [379, 377, 377, 377, 378, 379, 383, 389, 379], # times for 1 tile
    [388, 388, 388, 388, 388, 390, 394, 400, 396], # times for 2 tiles
    [399, 399, 399, 399, 399, 399, 411, 411, 421], # times for 3 tiles
    [406, 408, 408, 406, 406, 406, 418, 412, 426], # ...
    [419, 416, 418, 419, 419, 419, 431, 439, 435], # ...
    [426, 426, 426, 426, 426, 426, 439, 426, 444], # ...
    [438, 438, 438, 438, 436, 436, 451, 446, 457], # ...
    [446, 446, 446, 446, 446, 446, 460, 452, 462]  # times for 8 tiles
]

if __name__ == '__main__':
    t_overhead = []
    pc_overhead = []

    for n_tiles in timimg_data:
        overhead = stat.mean(n_tiles) - cl_render_time
        oh_pc = (overhead / cl_render_time) * 100
        pc_overhead.append(oh_pc)
        t_overhead.append(overhead)

    x_offset = 0.25
    width = 0.5
    num_tiles = len(timimg_data)
    t_normal = [cl_render_time] * num_tiles
    ind = [x_offset + x for x in range(num_tiles)]
    ind_position = [x + width/2.0 for x in ind]
    labels = [str(x) for x in range(1, num_tiles + 1)]

    fig = plt.figure(figsize=(12, 4))

    ################################################################################
    # plot 1 left
    ################################################################################

    plt.subplot(1, 2, 1)

    # y values for stacking are in t_normal and t_overhead
    p1 = plt.bar(ind, t_normal, width, color='#223d4a', linewidth=0)
    p2 = plt.bar(ind, t_overhead, width, color='#45b89d', bottom=t_normal, linewidth=0)

    plt.ylim(362, 450)
    plt.xticks(ind_position, labels)
    plt.yticks([cl_render_time, 450], [str(cl_render_time), '450'])
    plt.ylabel('time (s)')
    plt.xlabel('blocks')
    plt.title('mean overhead (s)')

    # mean command line render time
    plt.axhline(cl_render_time, color='#aaaaaa', zorder=5)

    # add labels above the bars: mean overhead time in seconds
    ax = plt.gca()
    rects = ax.patches
    for rect, label in zip(rects, t_overhead):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2, height + label + 3, "+{:.1f}s".format(label), \
            ha='center', va='bottom')

    # get rid of chartjunk
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    plt.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='on')
    plt.tick_params(axis='y', which='both', left='on', right='off', labelleft='on')


    ################################################################################
    # spacing
    ################################################################################
    plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=20)


    ################################################################################
    # plot 2 right
    ################################################################################
    plt.subplot(1, 2, 2)

    p1 = plt.bar(ind, pc_overhead, width, color='#45b89d', linewidth=0)

    plt.xticks(ind_position, labels)
    plt.xlabel('blocks')
    plt.title('mean overhead (%)')

    # add labels above the bars: mean overhead percentage
    ax = plt.gca()
    rects = ax.patches
    for rect, label in zip(rects, pc_overhead):
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2, height + 0.5, "{:.1f}%".format(label), \
            ha='center', va='bottom')

    # get rid of chartjunk
    ax.yaxis.set_ticks_position('left')
    ax.xaxis.set_ticks_position('bottom')
    ax.spines['left'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    plt.tick_params(axis='x', which='both', bottom='off', top='off', labelbottom='on')
    plt.tick_params(axis='y', which='both', left='off', right='off', labelleft='off')

    ################################################################################
    # overall title
    ################################################################################
    fig.suptitle('script overhead (10000 pixel image, single render node)', y=0.97, fontsize=16)
    plt.subplots_adjust(top=0.80, bottom=0.125)

    ################################################################################
    # plot and save
    ################################################################################
    fig.savefig('overhead_small.png', dpi=200)
