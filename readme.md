# Python code snippets

This repository contains a selection of unrelated short examples of Python 3 code.  All examples are complete and will run standalone.

Documentation for the snippets are contained in their respective directories.

|Directory|Function|
|:---:|:---:|
|[club](https://gitlab.com/skororu/pysnippets/tree/master/club)|A simple take on the classic developer recruitment test using [Flask](http://flask.pocoo.org) and [SQLite](https://sqlite.org)|
|[cycles](https://gitlab.com/skororu/pysnippets/tree/master/cycles)|An image rendering example with the [Resumable render implementation for Blender Cycles](https://developer.blender.org/rBf8b9f4e) using [NumPy](http://www.numpy.org) and [Pillow](https://python-pillow.org)|
|[matplotlib](https://gitlab.com/skororu/pysnippets/tree/master/matplotlib)|A few data visualisation examples, clear and chartjunk-free, using [matplotlib](http://matplotlib.org/)|
|[multiprocessing](https://gitlab.com/skororu/pysnippets/tree/master/multiprocessing)|Production line example using process-based parallelism with [multiprocessing](https://docs.python.org/3/library/multiprocessing.html)|
|[scrape](https://gitlab.com/skororu/pysnippets/tree/master/scrape)|A simple web scraping test using three different underlying methods of distributing the workload: single threaded, [ThreadPool](https://docs.python.org/2.7/library/multiprocessing.html#multiprocessing.pool.multiprocessing.Pool.imap_unordered) (with [Requests](http://docs.python-requests.org/en/master)), and [asyncio](https://docs.python.org/3/library/asyncio.html) (with [aiohttp](http://aiohttp.readthedocs.io/en/stable/)). [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup) is used to handle content processing.|
|[sierpinski](https://gitlab.com/skororu/pysnippets/tree/master/sierpinski)|A simple test of writing an array to an image using [NumPy](http://www.numpy.org) and [Pillow](https://python-pillow.org)|
|[zeromq](https://gitlab.com/skororu/pysnippets/tree/master/zeromq)|Communicate between a client and server using [ZeroMQ](http://zeromq.org) with tab completion and command history|
