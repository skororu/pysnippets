#!/usr/bin/env python3
"""
------------------------------------------------------------------------------

A simple take on the classic 'scrabble club' recruitment test using flask and
sqlite3

It avoids having to explicitly store all the played matches in the database,
and keeps database queries simple and efficient

Database commands conform to the command set as detailed here:
https://www.sqlite.org/lang.html

------------------------------------------------------------------------------

You will need: Python 3.5, Flask and sqlite3

Tested using Firefox on OS X - Safari seems to have issues with sessions not
persisting

(1) run this script in a terminal window
(2) direct your web browser to 127.0.0.1:5000

You will be able to:

* Add new members
* Edit existing members
* View members' profiles
* Add new matches
* Display leaderboard

and for debug purposes:

* display the entire contents of the database
* clear all match data from the database (leaving members intact)

------------------------------------------------------------------------------

the database schema is intentionally very simple, achieving the brief without
storing the matches, and allowing most operations to be accomplished with a
single query

+---------------+
| MATCHES       |
+---------------+
| ID            |
| NAME          |
| TELEPHONE     |
| WON           |
| LOST          |
| PLAYED        |
| AVERAGE_SCORE |
| HIGHEST_SCORE |
| MDATE         |
| LOCATION      |
| OPPONENT_ID   |
+---------------+

------------------------------------------------------------------------------
"""


import sqlite3  # connect
import flask    # Flask, Markup, redirect, render_template, request.form,
                # session, session.pop

app = flask.Flask(__name__)
app.secret_key = 'lumInousaQuaticmeSon_parKedcaT'
app.config["APPLICATION_ROOT"] = 'http://127.0.0.1:5000/'


###############################################################################
# utils - support for drop down menu handling
###############################################################################

def add_id(member_info, dups):
    """
    combines a member's name and ID if there is another member with the same
    name.

    provides a service for build_menu(), helps the user choose the
    correct member

    --------------------------------------------------------------------------
    args
        member_info : tuple (x, y)
            where:
            x = int, member ID
            y = string, member NAME
        dups : list of strings
            contains a list of names that are duplicated in the database
    --------------------------------------------------------------------------
    returns
        tuple (a, b) :
            where:
            a = string, member ID
            b = string, member NAME and (possibly) the ID
    --------------------------------------------------------------------------
    """
    m_id, m_name = member_info
    name_and_id = m_name + (' - ID ' + str(m_id) if m_name in dups else '')

    return str(m_id), name_and_id

def build_menu():
    """
    build an html drop down menu containing the names of all club members

    --------------------------------------------------------------------------
    args : none
    --------------------------------------------------------------------------
    returns
        markupsafe.Markup
    --------------------------------------------------------------------------
    """

    # identify members with duplicate names
    cursor = db.cursor()
    cursor.execute('''SELECT ID, NAME, COUNT(*)
        FROM MEMBERS
        GROUP BY NAME HAVING COUNT(*) > 1;''')
    db.commit()
    dup_names = [x[1] for x in cursor.fetchall()]

    # get all member names from the database and sort them (case insensitive)
    cursor = db.cursor()
    cursor.execute('''SELECT ID, NAME FROM MEMBERS ORDER BY NAME COLLATE NOCASE;''')
    db.commit()

    # build the drop down menu
    names = (add_id(row, dup_names) for row in cursor.fetchall())
    ddm = ''
    for m_id, m_name_id in names:
        ddm += '<option value="' + m_id + '">' + m_name_id + '</option>\n'

    return flask.Markup(ddm)


###############################################################################
# landing page
###############################################################################

@app.route('/')
def index():
    """ home page """

    return flask.render_template('index.html')


###############################################################################
# member: add
###############################################################################

@app.route('/member_add')
def member_add():
    """
    Add the name and contact details of a new club member

    Assume that members with the same name are different people, and therefore
    NAME will need to be read together with ID elsewhere in the code
    """

    return flask.render_template('member_add.html', title='Member: add new')

@app.route('/member_add_db', methods=['POST'])
def member_add_db():
    """ member: add new (process data from form) """

    name = flask.request.form['name']
    tel = flask.request.form['tel']

    cursor = db.cursor()
    cursor.execute('''INSERT INTO MEMBERS(NAME, TELEPHONE) VALUES(?,?);''', (name, tel))
    db.commit()

    return flask.redirect('/')


###############################################################################
# member: edit
###############################################################################

@app.route('/member_edit')
def member_edit():
    """ member: edit (display drop down menu form) """

    ddm = build_menu()
    dest = 'member_edit_details'

    return flask.render_template('member_select.html', \
        title='Member: edit', \
        drop_down=ddm, \
        dest=dest)

@app.route('/member_edit_details', methods=['POST'])
def member_edit_details():
    """ member: edit values (display edit form) """

    # this is the member id
    m_id = flask.request.form['myList']

    # get details of the member for editing
    cursor = db.cursor()
    cursor.execute('''SELECT NAME, TELEPHONE FROM MEMBERS WHERE ID = ?;''', (m_id,))
    db.commit()

    line = [x for x in cursor.fetchall()[0]]
    flask.session['m_id'] = m_id

    title = 'Member: edit details'
    nedit = line[0]
    tedit = line[1]

    return flask.render_template('member_edit_details.html', \
        title=title, nedit=nedit, tedit=tedit, m_id=m_id)

@app.route('/member_edit_details_2', methods=['POST'])
def member_edit_details_2():
    """ member: edit values (store) """

    name = flask.request.form['name']
    tel = flask.request.form['tel']
    m_id = flask.session.pop('m_id', None)

    cursor = db.cursor()
    cursor.execute('''UPDATE MEMBERS SET NAME=?, TELEPHONE=? WHERE ID=?;''', (name, tel, m_id))
    db.commit()

    return flask.redirect('/')


###############################################################################
# member: view profile
###############################################################################

@app.route('/member_view')
def member_view():
    """ member: view (display drop down menu form) """

    ddm = build_menu()
    dest = 'member_view_details'

    return flask.render_template('member_select.html', \
        title='Member: view', \
        drop_down=ddm, \
        dest=dest)

@app.route('/member_view_details', methods=['POST'])
def member_view_details():
    """ member: view details

    A member’s profile screen showing their
    number of wins
    number of losses
    their average score (implement with running mean)
    their highest score, when and where it was scored, and against whom
    """

    m_id = flask.request.form['myList']

    # get this members profile. use a second lookup to get the name of this
    # member's opponent's name from this table using a JOIN
    cursor = db.cursor()
    cursor.execute('''SELECT
        e.ID,
        e.NAME,
        e.WON,
        e.LOST,
        e.PLAYED,
        ROUND(e.AVERAGE_SCORE, 1) AS AVERAGE_SCORE,
        e.HIGHEST_SCORE,
        e.MDATE,
        e.LOCATION,
        e.OPPONENT_ID,
        m.NAME AS OPPONENT_NAME
        FROM MEMBERS e
        LEFT JOIN MEMBERS m ON e.OPPONENT_ID = m.ID
        WHERE e.ID = ?;''', (m_id,))
    db.commit()

    # sqlite3 cannot handle command line shell 'dot commands',
    # so manually prepend the database column titles
    data_rows = cursor.fetchall()
    column_titles = tuple(x[0] for x in cursor.description)
    data_rows.insert(0, column_titles)

    # build table in html form
    table = ''
    space = [4, 14, 7, 7, 7, 14, 14, 14, 11, 12, 14]
    for row in data_rows:
        table += ' '.join(str(x).rjust(i, '_') for x, i in zip(row, space)) + '<br>'

    table = flask.Markup(table)
    title = 'Member: view details'
    desc = flask.Markup('')

    return flask.render_template('view_table.html', title=title, table=table, desc=desc)


###############################################################################
# match: add new
###############################################################################

@app.route('/match_add')
def match_add():
    """
    match: add
    don't actually store matches, just update individual user records
    """

    ddm = build_menu()
    dest = 'match_add_details'

    return flask.render_template('match_add.html', \
        title='Match: add', \
        drop_down=ddm, \
        dest=dest)

@app.route('/match_add_details', methods=['POST'])
def match_add_details():
    """ match: add details """

    m1i = flask.request.form['member-1-id']
    m2i = flask.request.form['member-2-id']
    m1s = flask.request.form['member-1-score']
    m2s = flask.request.form['member-2-score']
    mlo = flask.request.form['match-location']
    mda = flask.request.form['match-date']

    if m1i == m2i:
        # ignore matches where a player plays themself
        return flask.redirect('/match_add')

    if m1s > m2s:
        # player 1 won
        core_update = ((m1s, 1, 0, m1i), (m2s, 0, 1, m2i))
    elif m1s < m2s:
        # player 2 won
        core_update = ((m1s, 0, 1, m1i), (m2s, 1, 0, m2i))
    else:
        # draw
        core_update = ((m1s, 0, 0, m1i), (m2s, 0, 0, m2i))

    best = ((m1s, mda, mlo, m2i, m1i, m1s), (m2s, mda, mlo, m1i, m2i, m2s))

    # for both players - update matches played, won, lost and running average
    for i in core_update:
        cursor = db.cursor()
        cursor.execute('''UPDATE MEMBERS
            SET
            AVERAGE_SCORE = (AVERAGE_SCORE * PLAYED + ?) / CAST((PLAYED + 1) AS FLOAT),
            PLAYED = PLAYED + 1,
            WON = WON + ?,
            LOST = LOST + ?
            WHERE ID=?;''', i)
        db.commit()

    # for both players - update best performance if appropriate
    for i in best:
        cursor.execute('''UPDATE MEMBERS
            SET
            HIGHEST_SCORE = ?,
            MDATE = ?,
            LOCATION = ?,
            OPPONENT_ID = ?
            WHERE ID=? AND HIGHEST_SCORE < ?;''', i)
        db.commit()

    return flask.redirect('/')

###############################################################################
# leaderboard
###############################################################################

@app.route('/leaderboard')
def leaderboard():
    """ debug: show database """

    cursor = db.cursor()
    cursor.execute('''SELECT ID, NAME, ROUND(AVERAGE_SCORE, 1) AS AVERAGE_SCORE
        FROM MEMBERS
        WHERE PLAYED >= 10
        ORDER BY AVERAGE_SCORE DESC
        LIMIT 10;''')
    db.commit()

    # sqlite3 cannot handle command line shell 'dot commands',
    # so manually prepend the database column titles
    data_rows = cursor.fetchall()
    column_titles = tuple(x[0] for x in cursor.description)
    data_rows.insert(0, column_titles)

    # build table in html form
    table = ''
    space = [4, 14, 14]
    for row in data_rows:
        table += ' '.join(str(x).rjust(i, '_') for x, i in zip(row, space)) + '<br>'

    table = flask.Markup(table)
    title = 'Leaderboard'
    desc = flask.Markup('<p>A list of club members with the top ten average scores, \
        for those who have played at least ten matches</p>')

    return flask.render_template('view_table.html', title=title, table=table, desc=desc)


###############################################################################
# debug
###############################################################################

@app.route('/admin_display_db')
def admin_display_db():
    """ show the contents of the entire database """

    # column titles listed individually instead of using '*' to allow the
    # float value of AVERAGE_SCORE to be formatted manually
    cursor = db.cursor()
    cursor.execute('''SELECT
        e.ID,
        e.NAME,
        e.TELEPHONE,
        e.WON,
        e.LOST,
        e.PLAYED,
        ROUND(e.AVERAGE_SCORE, 1) AS AVERAGE_SCORE,
        e.HIGHEST_SCORE,
        e.MDATE,
        e.LOCATION,
        e.OPPONENT_ID,
        m.NAME AS OPPONENT_NAME
        FROM MEMBERS e
        LEFT JOIN MEMBERS m ON e.OPPONENT_ID = m.ID;''')
    db.commit()

    # sqlite3 cannot handle command line shell 'dot commands',
    # so manually prepend the database column titles
    data_rows = cursor.fetchall()
    column_titles = tuple(x[0] for x in cursor.description)
    data_rows.insert(0, column_titles)

    # build table in html form
    table = ''
    space = [4, 14, 14, 7, 7, 7, 14, 14, 14, 11, 12, 14]
    for row in data_rows:
        table += ' '.join(str(x).rjust(i, '_') for x, i in zip(row, space)) + '<br>'

    table = flask.Markup(table)
    title = 'Database contents'
    desc = flask.Markup('<p>OPPONENT_NAME is derived from OPPONENT_ID to aid interpretation, \
        it is not directly stored in the database</p>')

    return flask.render_template('view_table.html', title=title, table=table, desc=desc)

@app.route('/admin_clear_matches')
def admin_clear_matches():
    """
    clears all match related data from the database,
    leaving member data intact
    """

    cursor = db.cursor()
    cursor.execute('''UPDATE MEMBERS
        SET
        AVERAGE_SCORE = 0.0,
        PLAYED = 0,
        WON = 0,
        LOST = 0,
        HIGHEST_SCORE = 0,
        MDATE = NULL,
        LOCATION = NULL,
        OPPONENT_ID = NULL;''')
    db.commit()

    return flask.redirect('/admin_display_db')


###############################################################################
# entry point
###############################################################################
if __name__ == '__main__':

    # if the database exists, connect to it
    # if the database doesn't exist, create it, then connect to it
    db = sqlite3.connect(r"club.db", check_same_thread=False)

    # add table if it isn't already present
    cursor = db.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS MEMBERS (
        ID INTEGER PRIMARY KEY,
        NAME TEXT,
        TELEPHONE TEXT,
        WON INTEGER DEFAULT 0,
        LOST INTEGER DEFAULT 0,
        PLAYED INTEGER DEFAULT 0,
        AVERAGE_SCORE FLOAT DEFAULT 0.0,
        HIGHEST_SCORE INTEGER DEFAULT 0,
        MDATE DATE,
        LOCATION TEXT,
        OPPONENT_ID TEXT);''')
    db.commit()

    # serve local http pages
    app.run(debug=True, host='0.0.0.0')
