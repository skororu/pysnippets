# Club

A classic recuitment task for developers is based around a simple database design and web interface.  This take pares back the database to its simplest form - eliminating the need to keep a record of all played matches - letting the database do the hard work, using straightforward queries, and using Python for the bare minimum.

##### How to get it working

* run the `club.py` script in a terminal window
* direct your web browser to 127.0.0.1:5000
* interact with the script using the browser

##### You will be able to:

* Add new members
* Edit existing members
* View members' profiles
* Add new matches
* Display leaderboard

and for debug purposes:

* display the entire contents of the database
* clear all match data from the database (leaving members intact)

```
__ID __________NAME _____TELEPHONE ____WON ___LOST _PLAYED _AVERAGE_SCORE _HIGHEST_SCORE _________MDATE ___LOCATION _OPPONENT_ID _OPPONENT_NAME
___1 ______Victoria ________175635 ______3 ______2 ______5 _________156.0 ___________181 ______6/2/2001 ______patio ___________6 _________Alice
___2 _______William ________123123 ______6 ______6 _____12 _________103.8 ___________132 _____16/7/2001 _____lounge ___________9 _________Roger
___3 _________James ________888899 ______3 ______2 ______5 _________141.0 ___________198 ______8/7/2001 _______park __________14 _________Polly
___4 ________Rupert ________000101 ______4 ______1 ______6 __________93.0 ___________149 _____17/5/1999 __reception ___________8 _________Polly
___5 _________Chloe ________111122 ______7 ______3 _____11 _________111.8 ___________164 ____11/10/2001 _______park ___________6 _________Alice
___6 _________Alice ________444454 ______4 ______8 _____12 _________126.0 ___________191 _____18/1/2001 _____lounge __________10 ________Martin
___7 _______Quentin ________665566 ______2 ______1 ______4 _________121.0 ___________162 _____3/11/2001 _______park ___________9 _________Roger
___8 _________Polly ________122112 ______3 ______7 _____11 _________124.4 ___________173 ______5/3/1999 ______woods __________14 _________Polly
___9 _________Roger ________789876 ______8 ______4 _____12 _________120.8 ___________150 _____17/2/1999 ____terrace ___________4 ________Rupert
__10 ________Martin ________440099 ______3 ______7 _____10 _________121.6 ___________171 ____23/12/2000 _____forest ___________8 _________Polly
__11 ________Sophia ________110011 ______3 ______0 ______3 __________94.3 ___________123 ____12/12/1999 _____garden __________10 ________Martin
__12 __________Mila ________661133 ______2 ______2 ______4 _________130.3 ___________145 ____18/10/2000 _____lounge ___________5 _________Chloe
__13 _______Heather ________111111 ______0 ______3 ______3 _________119.3 ___________176 _____26/3/2001 ______beach ___________7 _______Quentin
__14 _________Polly ________909090 ______1 ______3 ______4 _________164.8 ___________180 _____18/5/2001 ___clearing ___________5 _________Chloe
```
