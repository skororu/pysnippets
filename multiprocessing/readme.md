# multiprocessing

A quick test of using process-based parallelism with multiprocessing for a distributed rendering pipeline, where blocks are distributed to nodes for rendering, then collected and checked. The script deliberately fails some of the block checks to test returning blocks to the render queue.

There are two versions, `terse.py` and `verbose.py`. While they both perform the same task, the former's general structure is easier to understand at a glance, while the latter is commented more thoroughly and gives plenty of feedback to the user.  Both versions support an optional integer command line argument which specifies the number of nodes to use, if it is omitted a small default value is used.

##### How to get them working

Run each example with a command like `./verbose.py 6` or `for i in {1..10}; do time ./terse.py; done`
