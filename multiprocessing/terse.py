#!/usr/bin/env python3
"""
test of a suitable method to replace the rather serial main control loop
of dtr (*) as the script was blocking while network file transfers were
completed, delaying the start of rendering the next block.

(*) https://gitlab.com/skororu/dtr

simulated scenario:
    * 1 computer to run the script
    * 3 computers (nodes) to render the blocks

thus the script creates:
    * 3 processes, 1 for each node, running function 'render'
        to simulate despatching blocks to each node
    * 2 processes, running function 'collect'
        to simulate collecting completed blocks from nodes
    1 control loop that checks if the blocks have all been received

in essence the design carries out the renders and performs
fetching of completed blocks concurrently, allowing the the next block
render to start immediately - network file transfer is largely eliminated
as a source of overhead.

+-------+    +------+    +---------+                 +--------+
| BLOCK |>>>>|render|>>>>| COLLECT |                 | RETIRE |
| QUEUE |    +------+    | QUEUE   |    +-------+    | QUEUE  |
|       |        |       |         |>>>>|collect|>>>>|        |    +----------+
|       |    +------+    |         |    +-------+    |        |    | CHECK    |
|       |>>>>|render|>>>>|         |        |        |        |>>>>| COMPLETE |
|       |    +------+    |         |    +-------+    |        |    |          |
|       |        |       |         |>>>>|collect|>>>>|        |    +----------+
|       |    +------+    |         |    +-------+    |        |         |
|       |>>>>|render|>>>>|         |        |        |        |         |
+-------+    +------+    +---------+        |        +--------+         |
                 |                          |                           |
                 +-----<-----<-----<-----<--+--<-----<-----<-----<------+
                        RUN QUEUE (all processes terminate if not empty)
"""

import math                  # ceil
import multiprocessing as mp # Process, Queue
import queue                 # Empty exception
import random                # random
import sys                   # argv, exit
import time                  # sleep


##############################################################################
# processes
##############################################################################

def render(node, blocks, coll, terminate):
    """
    simulates despatching blocks to be rendered on remote nodes

    * get a block number from the block queue (blocks)
    * simulate time spent rendering
    * put block information [node, block] on the collection queue (coll)
    """
    while terminate.empty():
        try:
            block_num = blocks.get(timeout=1)
        except queue.Empty:
            continue
        else:
            time.sleep(node[1])
            coll.put([node[0], block_num])


def collect(blocks, coll, check, terminate):
    """
    simulates initiating collection of blocks from remote nodes

    * get a block number from the collection queue (collect)
    * simulate network file transfer time
    * put block information [node, block] on the check queue (check)
    """
    while terminate.empty():
        try:
            block_info = coll.get(timeout=1)
        except queue.Empty:
            continue
        else:
            time.sleep(random.random())

            # deliberately fail a few of the collected blocks
            if random.random() > 0.2:
                check.put(block_info)
            else:
                blocks.put(block_info[1])


##############################################################################
def main():
    """
    initialise concurrent processes and checking

    we will use one render process per node, as the processes will block
    during rendering.

    for collect processes, given that:

        * the time taken to render a block is very much greater than the time
              taken to transfer it over the network
        * variance in render times between nodes of similar performance is
              usually greater than the network transfer time
        * rendering and network transfers proceed concurrently

    it is thus unlikely we will need to handle many concurrent
    network file transfers, and even if a few are queued they will all be
    transferred long before the next render completes.

    in essence, we need less collect processes than render processes
    """

    ##########################################################################
    # user settings: get num_nodes from command line if present
    ##########################################################################
    try:
        num_nodes = int(sys.argv[1])
    except:
        num_nodes = 3

    num_blocks = num_nodes * 2

    ##########################################################################
    # initialise
    ##########################################################################

    blocks_q = mp.Queue()
    collect_q = mp.Queue()
    check_q = mp.Queue()
    run_q = mp.Queue()

    # fill queue with blocks to be rendered
    all_blocks = list(range(num_blocks))
    for block in all_blocks:
        blocks_q.put(block)

    # create some nodes with random performance data
    nodes = [[n, random.randint(1, 8)] for n in all_blocks]

    # initialise a render process for each node
    parfun = []
    for node in nodes:
        parfun.append(mp.Process(target=render, args=(node, blocks_q, collect_q, run_q)))

    # initialise an appropriate number of collect processes
    num_collectors = math.ceil(len(nodes) / 4)
    for i in range(num_collectors):
        parfun.append(mp.Process(target=collect, args=(blocks_q, collect_q, check_q, run_q)))

    # give the user basic configuration information
    print(num_blocks, 'blocks to be rendered on', num_nodes, 'nodes, using', \
        num_nodes, 'render processes and', num_collectors, 'collector processes')


    ##########################################################################
    # start
    ##########################################################################

    # start up render and collect processes
    for pfu in parfun:
        pfu.start()

    # check that all the blocks transfers have completed
    checked = []
    while len(checked) < len(all_blocks):
        try:
            block_info = check_q.get(timeout=1)
        except queue.Empty:
            continue
        else:
            checked.append(block_info[1])


    ##########################################################################
    # tidy up and exit
    ##########################################################################

    # indicate to render and collect processes that they can terminate now
    run_q.put(1)

    for pfu in parfun:
        pfu.join()

    if sorted(checked) != all_blocks:
        sys.exit("failed")


##############################################################################
if __name__ == '__main__':
    main()
